// Raqueta.h: interface for the Raqueta class.
// AUTOR: IGNACIO RAMOS GALINDO - 51434 - 2021/22
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
