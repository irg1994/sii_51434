# Changelog
Todos los cambios notables de este proyecto serán anotados en este documento.

## [1.1] 2021-10-06

### Added
-Añadido fichero Changelog.md

### Changed
-Modificadas todas las cabeceras del código fuente para añadir el autor.
